﻿public interface IPlayerController
{
    void Move(float x, float z);

    void FirstAttack();

    void SecondAttack();

    void Ability();
}
