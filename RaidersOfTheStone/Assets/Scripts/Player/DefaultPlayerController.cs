﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultPlayerController : MonoBehaviour, IPlayerController
{
    public float speed = 1f;

    Animator animator;
    Rigidbody rbody;

    void Start()
    {
        animator = GetComponent<Animator>();
        rbody = GetComponent<Rigidbody>();
    }
    void Update(){
        if(Input.GetAxis("Horizontal")!=0 || Input.GetAxis("Vertical")!=0)
        {
            Move(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        }
        if(Input.GetAxis("FirstAttack")!=0)
        {
            FirstAttack();
        }
        if (Input.GetAxis("SecondAttack") != 0)
        {
            SecondAttack();
        }
        if (Input.GetAxis("Ability") != 0)
        {
            Ability(); 
        }
    }

    public void Move(float x, float z){
        Vector3 movement = Vector3.ClampMagnitude(new Vector3(x, 0, z),1) * speed * Time.deltaTime; ;
        transform.Translate(movement,Space.World);
        float currentSpeed = movement.magnitude;
        print(currentSpeed);
        animator.SetFloat("speed", currentSpeed);
        transform.forward = movement;
    }

    public void FirstAttack(){
        
    }

    public void SecondAttack(){
        
    }

    public void Ability(){
        
    }
}
